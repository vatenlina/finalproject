FROM python:3.6
WORKDIR /root
RUN apt-get update && apt-get install python -y
RUN python --version
COPY . . 
CMD ["python3", "./fastai-docker-example/testing/test0.py" ]

